#!/usr/bin/env python3

# ---------------------
# testSpotifyAPITest.py
# ---------------------

# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase
import requests, base64, json, csv
from spotifyAPITest import getSpotifyAccessToken, get_genre, get_song_info, get_artist_info, get_album_info, write_csv
from secrets import *

# ------------------
# testSpotifyAPITest
# ------------------


class testSpotifyAPITest(TestCase):
    # ---------------------
    # getSpotifyAccessToken
    # ---------------------

    def test_getSpotifyAccessToken_1(self):
        # Access token is a string, we check if it is None and if it is an empty string
        accessToken = getSpotifyAccessToken(clientID, clientSecret)
        self.assertNotEqual(accessToken, None)
        self.assertNotEqual(accessToken, "")

    def test_getSpotifyAccessToken_2(self):
        # Check if accessToken works and find the right song with the songID
        accessToken = getSpotifyAccessToken(clientID, clientSecret)
        header = {"Authorization": "Bearer " + accessToken}
        getTrackURL = f"https://api.spotify.com/v1/tracks/3YJJjQPAbDT7mGpX3WtQ9A"
        songResult = requests.get(getTrackURL, headers=header).json()
        self.assertEqual(songResult['name'], "Good Days")
    # ---------
    # get_genre
    # ---------
    # use AudioDB
    def test_get_genre_1(self):
        # Check if accessToken works and find the right song with the songID
        # using audioDB, so the id doesn't matter
        genre = get_genre("Drake", "ssssss")
        self.assertEqual(genre, "Hip-Hop")

    # use spotifyAPI
    def test_get_genre_2(self):
        # Check if accessToken works and find the right song with the songID
        # using spotify API this one is not in audioDB and the id does mattter
        genre = get_genre("Intergalactical", "35Ne6Yhz7GK4tLBJSH8HHu")
        # the genre is unknown on spotify API
        self.assertEqual(genre, "Unknown")

    # # -------------
    # # get_song_info
    # # -------------
    #
    def test_get_song_info_1(self):
        # check if they could get the right answer
        song_info = get_song_info("3YJJjQPAbDT7mGpX3WtQ9A")
        self.assertEqual(song_info['name'], "Good Days")
        self.assertEqual(song_info['artist'], "SZA")
        self.assertEqual(song_info['artistID'], "7tYKF4w9nC0nq9CsPZTHyP")
        self.assertEqual(song_info['album'], "Good Days")
        self.assertEqual(song_info['albumID'], "781cKhbTPwLnPmo9BALQl7")
        # self.assertEqual(song_info['genres'], ['pop', 'pop rap'])
        flag = False
        if song_info['popularity'] >= 0 and song_info['popularity'] <= 100 and type(song_info['popularity']) is int:
            flag = True
        self.assertEqual(flag, True)
        self.assertEqual(song_info['duration_ms'], 279204)
        self.assertEqual(song_info['acousticness'], 0.499)
        self.assertEqual(song_info['danceability'], 0.436)
        self.assertEqual(song_info['energy'], 0.655)
        self.assertEqual(song_info['liveness'], 0.688)
        self.assertEqual(song_info['instrumentalness'], 8.1e-06)
        self.assertEqual(song_info['image'], "https://i.scdn.co/image/ab67616d0000b2733097b1375ab17ae5bf302a0a")

    # # ---------------
    # # get_artist_info
    # # ---------------

    def test_get_artist_info_1(self):
        artist_info = get_artist_info("1Xyo4u8uXC1ZmMpatF05PJ")
        self.assertEqual(artist_info['name'], "The Weeknd")
        # followers is always updated.
        # self.assertEqual(artist_info['followers'], 29917463)
        self.assertEqual(artist_info['intFormedYear'], '2010')
        self.assertEqual(artist_info['intMembers'], '1')
        # self.assertEqual(artist_info['strGenre'], "R&B")
        flag = False
        if artist_info['popularity'] >= 0 and artist_info['popularity'] <= 100 and type(artist_info['popularity']) is int:
            flag = True
        self.assertEqual(flag, True)
        self.assertEqual(artist_info['strArtistThumb'], "https://www.theaudiodb.com/images/media/artist/thumb/vtxqyp1495060298.jpg")
        self.assertEqual(artist_info['strCountry'],"Toronto, Canada")
        self.assertEqual(artist_info['strDisbanded'], "Present")
        self.assertEqual(artist_info['strTwitter'], "twitter.com/theweekndxo")
        self.assertEqual(artist_info['topID'], "5QO79kh1waicV47BqGRL3g")
        self.assertEqual(artist_info['topTrack'], "Save Your Tears")

    # # ---------------
    # # get_album_info
    # # ---------------
    def test_get_album_info_1(self):
        album_info = get_album_info("6OGzmhzHcjf0uN9j7dYvZH")
        self.assertEqual(album_info['name'], "Chanel")
        # self.assertEqual(album_info['genres'], "R&B")
        flag = False
        if album_info['popularity'] >= 0 and album_info['popularity'] <= 100 and type(album_info['popularity']) is int:
            flag = True
        self.assertEqual(flag, True)
        self.assertEqual(album_info['albumImage'], "https://i.scdn.co/image/ab67616d0000b273a0b780c23fc3c19bd412b234")
        self.assertEqual(album_info['artist'], "Frank Ocean")
        self.assertEqual(album_info['artistID'], "2h93pZq0e7k5yf4dywlkpM")
        self.assertEqual(album_info['label'], "Blonded")
        self.assertEqual(album_info['releaseDate'], "2017-03-10")
        self.assertEqual(album_info['totalTracks'], 1)

    # ---------------------
    # write_csv
    # ---------------------
    def test_write_csv_1(self):
        input = []
        dict1 = {"a": 13, "b": 14}
        dict2 = {"a": 15, "b": 16}
        input.append(dict1)
        input.append(dict2)
        inputFieldNames = ["a", "b"]
        write_csv(input, inputFieldNames, "test_write_csv.csv")
        result = []
        with open("test_write_csv.csv") as csvFile:  # open the file
            CSVdata = csv.reader(csvFile, delimiter=',')  # read the data
            for row in CSVdata:  # loop through each row
                result.append(row)  # print the data
        csvFile.close()
        self.assertEqual(result[0], ['13', '14'])
        self.assertEqual(result[1], ['15', '16'])


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
