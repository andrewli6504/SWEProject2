from flask import Flask, request, make_response, send_from_directory, render_template, Blueprint
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import or_
from flask_cors import CORS
from models import Songs, Artists, Albums, app, db

def query_safe(key, queryDict):
    try:
        return queryDict[key]
    except:
        return None

def get_artist_json(artist):
    return {
        "name" : artist.name,
        "artistID" : artist.artistID,
        "followers" : artist.followers, 
        "popularity" : artist.popularity,
        "topTrack" : artist.topTrack,
        "topID" : artist.topID,
        "topAlbum" : artist.topAlbum,
        "topAlbumID" : artist.topAlbumID,
        "strGenre" : artist.strGenre,
        "intFormedYear" : artist.intFormedYear,
        "strDisbanded" : artist.strDisbanded,
        "strCountry" : artist.strCountry,
        "intMembers" : artist.intMembers,
        "strArtistThumb" : artist.strArtistThumb,
        "strTwitter" : artist.strTwitter
    }


def filter_artists(query, args):
    intFormedYear = query_safe("intFormedYear", args)
    strGenre = query_safe("strGenre", args)
    popularity = query_safe("popularity", args)
    intMembers = query_safe("intMembers", args)

    if intFormedYear:
        query = query.filter(Artists.intFormedYear >= (intFormedYear[0]))
        query = query.filter(Artists.intFormedYear <= (intFormedYear[1]))

    if strGenre:
        query = query.filter(Artists.strGenre.in_(strGenre))

    if popularity:
        query = query.filter(Artists.popularity >= (popularity[0]))
        query = query.filter(Artists.popularity <= (popularity[1]))

    if intMembers:
        query = query.filter(Artists.intMembers >= (intMembers[0]))
        query = query.filter(Artists.intMembers <= (intMembers[1]))

    return query

def sort_artists(query, sort, direction):
    toSort = Artists.name
    if sort == "name":
        toSort = Artists.name
    elif sort == "intMembers":
        toSort = Artists.intMembers
    elif sort == "strGenre":
        toSort = Artists.strGenre
    elif sort == "popularity":
        toSort = Artists.popularity
    elif sort == "intFormedYear":
        toSort = Artists.intFormedYear
    
    if direction:
        return query.order_by(toSort.desc())
    else:
        return query.order_by(toSort.asc())



def search_artists(search, query_results):
    if search is None or len(search) < 1:
        return query_results
    search = search[0].strip()
    keywords = search.split()

    results = []
    for word in keywords:
        # contains might not work ?
        results.append(Artists.name.like("%"+word+"%"))
        results.append(Artists.strCountry.like("%"+word+"%"))
        results.append(Artists.topTrack.like("%"+word+"%"))
        results.append(Artists.topAlbum.like("%"+word+"%"))
        results.append(Artists.strGenre.like("%"+word+"%"))
    query_results = query_results.filter(or_(*tuple(results)))

    return query_results 
