from flask import Flask, request, make_response, send_from_directory, render_template, Blueprint
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import or_
from flask_cors import CORS
from models import Songs, Artists, Albums, app, db, api

def query_safe(key, queryDict):
    try:
        return queryDict[key]
    except:
        return None

def get_song_json(song):
    return {
        "name" : song.name,
        "songID" : song.nameID,
        "artist" : song.artist, 
        "artistID" : song.artistID,
        "album" : song.album,
        "albumID" : song.albumID,
        "genres" : song.genres,
        "popularity" : song.popularity,
        "duration_ms" : song.duration_ms,
        "acousticness" : song.acousticness,
        "danceability" : song.danceability,
        "energy" : song.energy,
        "liveness" : song.liveness,
        "instrumentalness" : song.instrumentalness,
        "image" : song.image
    }


def filter_songs(query, args):
    artist = query_safe("artist", args)
    genres = query_safe("genres", args)
    popularity = query_safe("popularity", args)
    duration_ms = query_safe("duration_ms", args)

    if artist:
        query = query.filter(Songs.artist.in_(artist))

    if genres:
        query = query.filter(Songs.genres.in_(genres))

    if popularity:
        query = query.filter(Songs.popularity >= (popularity[0]))
        query = query.filter(Songs.popularity <= (popularity[1]))

    if duration_ms:
        query = query.filter(Songs.duration_ms >= (duration_ms[0]))
        query = query.filter(Songs.duration_ms <= (duration_ms[1]))

    return query

def sort_songs(query, sort, direction):
    toSort = Songs.name
    if sort == "name":
        toSort = Songs.name
    elif sort == "artist":
        toSort = Songs.artist
    elif sort == "genres":
        toSort = Songs.genres
    elif sort == "popularity":
        toSort = Songs.popularity
    elif sort == "duration_ms":
        toSort = Songs.duration_ms
    
    if direction:
        return query.order_by(toSort.desc())
    else:
        return query.order_by(toSort.asc())



def search_songs(search, query_results):
    if search is None or len(search) < 1:
        return query_results
    search = search[0].strip()
    keywords = search.split()

    results = []
    for word in keywords:
        # contains might not work ?
        results.append(Songs.name.like("%"+word+"%"))
        results.append(Songs.artist.like("%"+word+"%"))
        results.append(Songs.album.like("%"+word+"%"))
        results.append(Songs.genres.like("%"+word+"%"))
    query_results = query_results.filter(or_(*tuple(results)))

    return query_results 
