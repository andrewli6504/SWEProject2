import React from 'react';
import * as Enzyme from 'enzyme';
import { shallow, render } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

// Components
import AboutPage from './components/Pages/About/AboutPage'
import AboutAPISets from './components/Pages/About/AboutAPISets'
import AboutTools from './components/Pages/About/AboutTools';
import HomePage from './components/Pages/Home/HomePage';
import AlbumsPage from './components/Pages/Albums/AlbumsPage';
import AlbumCard from './components/Cards/AlbumCard';
import ArtistsPage from './components/Pages/Artists/ArtistsPage'
import ArtistInstancePage from './components/Pages/Artists/ArtistInstancePage'
import SongsPage from './components/Pages/Songs/SongsPage'
import SongInstancePage from './components/Pages/Songs/SongInstancePage'

Enzyme.configure({ adapter: new Adapter() })

describe('About Page Test Suite', () => {
    it('About Page: NavBar', async () => {
        const component = shallow(<AboutPage />)
        expect(component).not.toBeUndefined();
        expect(component).toHaveLength(1);
        expect(component.find("NavBar")).toHaveLength(1);
    })
});

describe('About Page Test Suite', () => {
    it('About Page: AboutCards', async () => {
        const component = shallow(<AboutPage />)
        expect(component).not.toBeUndefined();
        expect(component).toHaveLength(1);
        expect(component.find("AboutCard")).toHaveLength(5);
    })
});

describe('About Page Test Suite', () => {
    it('About Page: Image', async () => {
        const component = shallow(<AboutPage />)
        expect(component).not.toBeUndefined();
        expect(component).toHaveLength(1);
        expect(component.find("Image")).toHaveLength(3);
    })
});

describe('About APISets Test Suite', () => {
    it('About APISets', async () => {
        const component = shallow(<AboutAPISets />)
        expect(component).not.toBeUndefined();
        expect(component).toHaveLength(1);
        expect(component.find("div.text-center")).toHaveLength(1);
    })
});

describe('About Tools Test Suite', () => {
    it('About Tools', async () => {
        const component = shallow(<AboutTools />)
        expect(component).not.toBeUndefined();
        expect(component).toHaveLength(1);
        expect(component.find("div.text-center")).toHaveLength(1);
    })
});

describe('Home Page Test Suite', () => {
    it('Home Page', async () => {
        const component = shallow(<HomePage />)
        expect(component).not.toBeUndefined();
        expect(component).toHaveLength(1);
        expect(component.find("div.home-page")).toHaveLength(1);
    })
});

describe('Albums Page Test Suite', () => {
    it('Album Page: NavBar', async () => {
        const component = shallow(<AlbumsPage />)
        expect(component).not.toBeUndefined();
        expect(component).toHaveLength(1);
        expect(component.find("NavBar")).toHaveLength(1);
    })
});

describe('Albums Page Test Suite', () => {
    it('Album Page: Pagination', async () => {
        const component = shallow(<AlbumsPage />)
        expect(component).not.toBeUndefined();
        expect(component).toHaveLength(1);
        expect(component.find("Pagination")).toHaveLength(1);
        
    })
});

describe('Albums Page Test Suite', () => {
    it('Album Page: Fragment', async () => {
        const component = shallow(<AlbumsPage />)
        expect(component).not.toBeUndefined();
        expect(component).toHaveLength(1);
        expect(component.find("Fragment")).toHaveLength(1);
    })
});

describe('Album Card Test Suite', () => {
    it('Album Card: Link', async () => {
        const component = shallow(<AlbumCard data={name="poggers"}/>)
        expect(component).not.toBeUndefined();
        expect(component).toHaveLength(1);
        expect(component.find("Link")).toHaveLength(1);
    })
});

describe('Album Card Test Suite', () => {
    it('Album Card: div', async () => {
        const component = shallow(<AlbumCard data={name="poggers"}/>)
        expect(component).not.toBeUndefined();
        expect(component).toHaveLength(1);
        expect(component.find("div")).toHaveLength(2);
    })
});

describe('Album Card Test Suite', () => {
    it('Album Card: Image', async () => {
        const component = shallow(<AlbumCard data={name="poggers"}/>)
        expect(component).not.toBeUndefined();
        expect(component).toHaveLength(1);
        expect(component.find("img")).toHaveLength(1);
    })
});

describe('Artists Page Test Suite', () => {
    it('Artists Page', async () => {
        const component = render(<ArtistsPage />)
        expect(component).not.toBeUndefined();
        expect(component).toHaveLength(1);
        expect(component.find("table")).toHaveLength(1);
        expect(component.find("span.react-bootstrap-table-pagination-total")).not.toBeUndefined();
    })
});

describe('Songs Page Test Suite', () => {
    it('Songs Page', async () => {
        const component = render(<SongsPage />)
        expect(component).not.toBeUndefined();
        expect(component).toHaveLength(1);
        expect(component.find("table")).toHaveLength(1);
        expect(component.find("span.react-bootstrap-table-pagination-total")).not.toBeUndefined();
    })
});
