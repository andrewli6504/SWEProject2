import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from 'react-router-dom' 
import App from "./App";

import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById("root")
);
