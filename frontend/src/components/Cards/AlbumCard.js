import React, {useState} from "react"
import { Checkbox} from "antd"
import { Link } from "react-router-dom"
import "../Pages/Albums/AlbumsPage.css"
import Highlight from "react-highlighter"

const AlbumCard = ({ data, search, callback }) => {
	const { name, albumID, artist, genres, popularity, totalTracks, albumImage } = data
	const [checked, setChecked] = useState(false)
	const onChange = (event) => {
		setChecked(callback(name, albumID))
	}

	return (
		<div class="card bg-light mb-3">
			<img class="card-img-top" src={albumImage} alt={name}></img>
			<div class="card-body">
				<h3 class="card-title" maxlength="25" style={{ overflow: "hidden", textOverflow: "ellipsis" }}>
					<Highlight search={search}>{name}</Highlight>
				</h3>
				<h4 class="card-title">
					<Highlight search={search}>{artist}</Highlight>
				</h4>
				<p class="card-text">
					Genre: <Highlight search={search}>{genres === "" ? "Unknown" : genres}</Highlight>
				</p>
				<p class="card-text">Popularity: {popularity}</p>
				<p class="card-text">Tracks: {totalTracks}</p>
				<Link key={albumID} to={`/album/${albumID}`}>
					More Info
				</Link>
				<hr class="my-2"></hr>
				<p class="card-text">Compare: <Checkbox checked={checked} onChange={onChange}></Checkbox></p>
			</div>
		</div>
	)
}

export default AlbumCard
