import React from "react";
import { Card } from "react-bootstrap";

function AboutCard(props) {
    return (
        <div className="text-center">
            <Card className='mt-3'>
                <a id={props.member.gitlabID + "_img"} href={props.member.linkedin}>
                    <Card.Img variant="top" src={props.member.image} className="img-fluid" />
                </a>
                <Card.Body>
                    <a href={props.member.linkedin}>
                        <Card.Title>
                            {props.member.name}
                        </Card.Title>
                    </a>
                    <Card.Text>
                        {props.member.role}
                        <br />
                        {props.member.bio}
                        <br />
                        <div className="container">
                            <div className="row justify-content-around">
                                <div className="col-md-4 col-sm-12">
                                    Issues:
                                        <br />
                                    {props.issues === -1 ? "" : props.issues}
                                </div>
                                <div className="col-md-4 col-sm-6">
                                    Commits:
                                        <br />
                                    {props.commits === -1 ? "" : props.commits}
                                </div>
                                <div className="col-md-4 col-sm-6">
                                    Unit Tests:
                                        <br />
                                    {props.member.unittests}
                                </div>
                            </div>
                        </div>
                    </Card.Text>
                </Card.Body>
            </Card>
        </div>
    )
}

export default AboutCard;