import React from "react"
import { Card, Image } from "react-bootstrap"
import Highlight from "react-highlighter"

function SongSearchCard(props) {
	return (
		<div className="text-left">
			<Card className="mx-auto" style={{ width: "30rem" }}>
				<a href={"https://spotifynder.com/song/" + props.song.songID}>
					<div className="container">
						<div className="row d-flex flex-wrap align-items-center">
							<div className="col-md-6">
								<Card.Body className="text-dark">
									<a href={"https://spotifynder.com/song/" + props.song.songID}>
										<Card.Title className="text-">
											<Highlight search={props.search}>{props.song.name}</Highlight>
										</Card.Title>
									</a>
									<Card.Text>
										Artist: <Highlight search={props.search}>{props.song.artist}</Highlight>
									</Card.Text>
									<Card.Text>
										Album: <Highlight search={props.search}>{props.song.album}</Highlight>
									</Card.Text>
									<Card.Text>
										Genre: <Highlight search={props.search}>{props.song.genres}</Highlight>
									</Card.Text>
								</Card.Body>
							</div>
							<div className="col-md-6">
								<Image src={props.song.image} alt={props.song.album + " Album Cover"} fluid thumbnail />
							</div>
						</div>
					</div>
				</a>
			</Card>
		</div>
	)
}

export default SongSearchCard
