import React, { useEffect, useState } from "react"
import { Image } from "react-bootstrap"
import AboutCard from "../../Cards/AboutCard"
import NavBar from "../../NavBar"
import AboutAPISets from "./AboutAPISets"
import AboutTools from "./AboutTools"
import YT from "../../Images/yt.png"

import "./About.css"

const nathan = {
	id: 6945833,
	index: 0,
}

const connor = {
	id: 6164268,
	index: 1,
}

const mengning = {
	id: 5011150,
	index: 2,
}

const andrew = {
	id: 7665462,
	index: 3,
}

const blake = {
	id: 7665477,
	index: 4,
}

function AboutPage() {
	const [nathanIssues, setNathanIssues] = useState(-1)
	const [connorIssues, setConnorIssues] = useState(-1)
	const [mengningIssues, setMengningIssues] = useState(-1)
	const [andrewIssues, setAndrewIssues] = useState(-1)
	const [blakeIssues, setBlakeIssues] = useState(-1)
	const [commits, setCommits] = useState([-1, -1, -1, -1, -1])

	const [members] = useState([
		{
			name: "Nathan Jackson",
			gitlabID: "nathanljackson112",
			email: "nathanljackson@att.net",
			linkedin: "https://www.linkedin.com/in/nathanljackson/",
			role: "Frontend Developer",
			bio: "Hi I am Nathan, a junior CS major. I worked on the frontend and infrastructure. I enjoy baking, football, and playing D&D.",
			commits: commits[0],
			issues: nathanIssues,
			unittests: 27,
			image: "https://i.imgur.com/6iN0xc2.jpg",
		},
		{
			name: "Connor Kite",
			gitlabID: "ckite2k",
			email: "connor.kite2000@gmail.com",
			linkedin: "https://www.linkedin.com/in/connor-kite-939647184/",
			role: "Backend Developer",
			bio: "Hi I'm Connor, a senior CS major, and like to play games and watch movies when I get some free time in between projects.",
			commits: commits[1],
			issues: connorIssues,
			unittests: 18,
			image: "https://i.imgur.com/CHt8sV8.jpg",
		},
		{
			name: "Mengning Geng",
			gitlabID: "mengning",
			email: "mengninggeng@utexas.edu",
			linkedin: "https://www.linkedin.com/in/mengning-geng-459a4a10a/",
			role: "Backend Developer",
			bio: "Hi I'm Mengning, a senior CS major, and I like swimming and cooking especially during the Covid-19 pandemic.",
			commits: commits[2],
			issues: mengningIssues,
			unittests: 8,
			image: "https://i.imgur.com/xFuA2Ja.jpg",
		},
		{
			name: "Andrew Li",
			gitlabID: "andrewli6504",
			email: "andrewli6504@utexas.edu",
			linkedin: "https://www.linkedin.com/in/andrew-li-37b389159/",
			role: "Frontend Developer",
			bio: "Hi, I'm Andrew, a second year CS major, and my favorite way to spend my free time is to sit down with friends and play a board game.",
			commits: commits[3],
			issues: andrewIssues,
			unittests: 10,
			image: "https://i.imgur.com/3dzSn0G.jpg",
		},
		{
			name: "Blake Romero",
			gitlabID: "BlakeRomero",
			email: "blakeromero@utexas.edu",
			linkedin: "https://www.linkedin.com/in/blake-romero-030a011b7/",
			role: "Frontend Developer",
			bio: "Hi, I'm Blake, a second year CS major. I'm from Houston, and I like to spend my free time playing video games and board games with friends.",
			commits: commits[4],
			issues: blakeIssues,
			unittests: 10,
			image: "https://i.imgur.com/e52HkbM.jpg",
		},
	])

	useEffect(() => {
		const requestOptions = {
			method: "GET",
			headers: {
				"PRIVATE-TOKEN": "k72j3ziqVuY_GGRmmX4b",
			},
		}
		async function getCommitsData() {
			const response = await fetch("https://gitlab.com/api/v4/projects/24679643/repository/contributors", requestOptions)
			const data = await response.json()
			let newCommits = [0, 0, 0, 0, 0]
			for (let i = 0; i < data.length; i++) {
				switch (data[i].name) {
					case "Nathan Jackson":
					case "nathanljackson112":
						newCommits[nathan.index] += data[i].commits
						break
					case "connor":
					case "Connor Kite":
						newCommits[connor.index] += data[i].commits
						break
					case "Andrew Li":
					case "andrewli6504":
						newCommits[andrew.index] += data[i].commits
						break
					case "Mengning Geng":
					case "耿梦宁":
						newCommits[mengning.index] += data[i].commits
						break
					case "BlakeRomero":
					case "Blake Romero":
						newCommits[blake.index] += data[i].commits
						break
					default:
						break
				}
			}
			setCommits(newCommits)
		}
		async function getNathanIssuesData() {
			const response = await fetch(`https://gitlab.com/api/v4/projects/24679643/issues_statistics?author_id=${nathan.id}`, requestOptions)
			const data = await response.json()
			setNathanIssues(data.statistics.counts.all)
		}
		async function getConnorIssuesData() {
			const response = await fetch(`https://gitlab.com/api/v4/projects/24679643/issues_statistics?author_id=${connor.id}`, requestOptions)
			const data = await response.json()
			setConnorIssues(data.statistics.counts.all)
		}
		async function getMengningIssuesData() {
			const response = await fetch(`https://gitlab.com/api/v4/projects/24679643/issues_statistics?author_id=${mengning.id}`, requestOptions)
			const data = await response.json()
			setMengningIssues(data.statistics.counts.all)
		}
		async function getAndrewIssuesData() {
			const response = await fetch(`https://gitlab.com/api/v4/projects/24679643/issues_statistics?author_id=${andrew.id}`, requestOptions)
			const data = await response.json()
			setAndrewIssues(data.statistics.counts.all)
		}
		async function getBlakeIssuesData() {
			const response = await fetch(`https://gitlab.com/api/v4/projects/24679643/issues_statistics?author_id=${blake.id}`, requestOptions)
			const data = await response.json()
			setBlakeIssues(data.statistics.counts.all)
		}
		getCommitsData()
		getNathanIssuesData()
		getConnorIssuesData()
		getMengningIssuesData()
		getAndrewIssuesData()
		getBlakeIssuesData()
	}, [])

	return (
		<div>
			<NavBar />
			<br />
			<h1 className="about">About Us</h1>
			<br />
			<div className="row align-items-center justify-content-center" margin="auto">
				<div className="col-6">
					<h5 className="text-center">Spotifynder is a service built to view in-depth statistics about the songs, artists, and albumns you listen to.</h5>
					<h5 className="text-center">Our goal is not only to provide our users with a powerful database to find this information, but to also empower the discovery of smaller creators with the ability to search for music that is similar to the songs they love by less well known artists.</h5>
					<h5 className="text-center">We encourage you to explore our website and "fynd" something new that you may enjoy!</h5>
				</div>
			</div>
			<br />
			<div className="text-center">
				<h2>Meet the Team</h2>
			</div>
			<div className="container">
				<div className="row justify-content-around">
					<div className="col-4">
						<AboutCard member={members[0]} commits={commits[0]} issues={nathanIssues} />
					</div>
					<div className="col-4">
						<AboutCard member={members[1]} commits={commits[1]} issues={connorIssues} />
					</div>
					<div className="col-4">
						<AboutCard member={members[2]} commits={commits[2]} issues={mengningIssues} />
					</div>
				</div>
				<div className="row justify-content-center">
					<div className="col-4">
						<AboutCard member={members[3]} commits={commits[3]} issues={andrewIssues} />
					</div>
					<div className="col-4">
						<AboutCard member={members[4]} commits={commits[4]} issues={blakeIssues} />
					</div>
				</div>
			</div>
			<div>
				<br />
				<div className="container">
					<div className="row justify-content-center">
						<div className="col-md-3">
							<h4 className="text-center">Total Issues: {nathanIssues + connorIssues + mengningIssues + andrewIssues + blakeIssues === -5 ? "" : nathanIssues + connorIssues + mengningIssues + andrewIssues + blakeIssues}</h4>
						</div>
						<div className="col-md-3">
							<h4 className="text-center">Total Commits: {commits[0] + commits[1] + commits[2] + commits[3] + commits[4] === -5 ? "" : commits[0] + commits[1] + commits[2] + commits[3] + commits[4]}</h4>
						</div>
						<div className="col-md-3">
							<h4 className="text-center">Total Unit Tests: {members[0].unittests + members[1].unittests + members[2].unittests + members[3].unittests + members[4].unittests}</h4>
						</div>
					</div>
				</div>
			</div>
			<br />
			<div className="container">
				<div className="row justify-content-center align-items-center">
					<div className="col-md-4">
						<h3 className="text-center">
							<a id="youtube_link" href={"https://www.youtube.com/watch?v=sfkRZMK8e7U"}>
								<Image src={YT} width="200 px" height="200px" fluid />
								<br />
								<u>Presentation</u>
							</a>
						</h3>
					</div>
					<div className="col-md-4">
						<h3 className="text-center">
							<a id="postman_link" href={"https://documenter.getpostman.com/view/14731082/Tz5jefMk"}>
								<Image src={"https://seeklogo.com/images/P/postman-logo-F43375A2EB-seeklogo.com.png"} width="200 px" height="200px" fluid />
								<br />
								<u>Postman API</u>
							</a>
						</h3>
					</div>
					<div className="col-md-4">
						<h3 className="text-center">
							<a id="gitlab_link" href={"https://gitlab.com/ckite2k/SWEProject2"}>
								<Image src={"https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/GitLab_Logo.svg/1200px-GitLab_Logo.svg.png"} width="200 px" height="200px" fluid />
								<br />
								<u>Gitlab Repo</u>
							</a>
						</h3>
					</div>
				</div>
			</div>
			<br />
			<AboutAPISets />
			<br />
			<AboutTools />
			<br />
		</div>
	)
}

export default AboutPage
