import React from "react"
import { useState } from "react"
import Navbar from "react-bootstrap/Navbar"
import Nav from "react-bootstrap/Nav"
import Button from "react-bootstrap/Button"
import "./HomePage.css"
import logo from "../../Images/logo192.png"
import Form from "react-bootstrap/Form"
import FormControl from "react-bootstrap/FormControl"

function HomePage() {
	const [query, setQuery] = useState("")

	return (
		<div className="home-page">
			<div background-color="#13da0e">
				<div>
					<Navbar fixed="top" bg="primary" variant="dark">
						<Navbar.Brand href="/">
							<img src={logo} width="30" height="30" hspace="10" className="d-inline-block align-top" alt="Spotifynder Logo" />
							Spotifynder
						</Navbar.Brand>
						<Nav className="mr-auto">
							<Nav.Link id="home_link" href="/">
								Home
							</Nav.Link>
							<Nav.Link id="songs_link" href="/songs">
								Songs
							</Nav.Link>
							<Nav.Link id="artists_link" href="/artists">
								Artists
							</Nav.Link>
							<Nav.Link id="albums_link" href="/albums">
								Albums
							</Nav.Link>
							<Nav.Link id="about_link" href="/about">
								About
							</Nav.Link>
							<Nav.Link id="vis_link" href="/visualizations">
								Visualizations
					</Nav.Link>
						</Nav>

						<Form inline>
							<FormControl
								onChange={(event) => {
									setQuery(event.target.value)
								}}
								type="text"
								placeholder="Search"
								className="mr-auto"
							/>
							<Button variant="info" href={"/search/" + query}>
								Search
							</Button>
						</Form>
					</Navbar>
				</div>
				<div className="intro">
					<div class="container-fluid d-flex align-items-center justify-content-center vh-100">
						<div class="row d-flex justify-content-center text-center">
							<div class="col-md-12">
								<h2 class="text-light display-4 font-weight-bold pt-5 mb-2 text-centered">Spotifynder</h2>
								<br />
								<div className="row justify-content-center align-items-start" fluid>
									<div className="col-md-4 justify-content-center text-center">
										<Button variant="primary" size="lg" id="songs_nav" href="/songs">
											Songs
										</Button>
									</div>
									<div className="col-md-4 justify-content-center text-center">
										<Button variant="primary" size="lg" id="artists_nav" href="/artists">
											Artists
										</Button>
									</div>
									<div className="col-md-4 justify-content-center text-center">
										<Button variant="primary" size="lg" id="albums_nav" href="/albums">
											Albums
										</Button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}

export default HomePage
{/*  */}