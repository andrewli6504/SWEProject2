import React from "react"

function ErrorPage() {
	return <h1>Oops! Page not found. Check your url!</h1>
}

export default ErrorPage
