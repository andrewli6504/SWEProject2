import React, { useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import NavBar from "../../NavBar"
const https = require("https")
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"

function ArtistComparePage() {
	const { artist1ID, artist2ID } = useParams()
	const [artist1Data, setArtist1Data] = useState({})
	const [artist2Data, setArtist2Data] = useState({})

	const songLink1 = `/song/${artist1Data.topID}`
	const albumLink1 = `/album/${artist1Data.topAlbumID}`

	const songLink2 = `/song/${artist2Data.topID}`
	const albumLink2 = `/album/${artist2Data.topAlbumID}`

	const getArtistData = async () => {
		try {
			const httpsAgent = new https.Agent({
				rejectUnauthorized: false,
			})
			const requestOptions = {
				method: "GET",
				agent: httpsAgent,
				headers: {},
			}
			const response1 = await fetch(`https://spotifynder.com/api/artists?artistID=${artist1ID}`, requestOptions)
			const data1 = await response1.json()
			setArtist1Data(data1.message[0])
            
			const response2 = await fetch(`https://spotifynder.com/api/artists?artistID=${artist2ID}`, requestOptions)
			const data2 = await response2.json()
			setArtist2Data(data2.message[0])
		} catch (e) {
			console.error(e)
		}
	}

	function addCommas(nStr) {
		nStr += ""
		let x = nStr.split(".")
		let x1 = x[0]
		let x2 = x.length > 1 ? "." + x[1] : ""
		let rgx = /(\d+)(\d{3})/
		while (rgx.test(x1)) {
			x1 = x1.replace(rgx, "$1,$2")
		}
		return x1 + x2
	}

	useEffect(() => {
		getArtistData()
	})

	return (
		<div>
			<NavBar />
			<a href="/artists" class="btn btn-primary" style={{ marginTop: "15px", marginLeft: "15px" }}>
				Back to Artists
			</a>
			<br/>
			<div className="container" style={{display: "flex"}}>
				<div class="header" style={{ margin: "auto", display: "flex", justifyContent: "center" }}>
					<div class="card mb-3" style={{ width: "500px" }}>
						<h2 class="card-header">{artist1Data.name}</h2>
						<div class="card-body">
							<h5 class="card-title">Followers: {addCommas(artist1Data.followers)}</h5>
						</div>
						<img class="artist-image" src={artist1Data.strArtistThumb} alt="" />
						<div class="card-body">
							<div className="container">
								<div className="row justify-content-around">
									<div className="col-md-6">
										<p class="card-text">
											Active Years: {artist1Data.intFormedYear} - {artist1Data.strDisbanded}
										</p>
									</div>
									<div className="col-md-6">
										<p class="card-text">Genre: {artist1Data.strGenre}</p>
									</div>
								</div>
								<br />
								<div className="row justify-content-around">
									<div className="col-md-6">
										<p class="card-text">Members: {artist1Data.intMembers}</p>
									</div>
									<div className="col-md-6">{artist1Data.strCountry !== "" && <p class="card-text">From: {artist1Data.strCountry}</p>}</div>
								</div>
								<br />
								<div className="row justify-content-around">
									<div className="col-md-6">
										<p class="card-text">
											Top Track:{" "}
											<a href={songLink1} id="top-track" class="card-link">
												{artist1Data.topTrack}
											</a>
										</p>
									</div>
									<div className="col-md-6">
										<p class="card-text">
											Top Album:{" "}
											<a href={albumLink1} id="top-album" class="card-link">
												{artist1Data.topAlbum}
											</a>
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="header" style={{ margin: "auto", display: "flex", justifyContent: "center" }}>
					<div class="card mb-3" style={{ width: "500px" }}>
						<h2 class="card-header">{artist2Data.name}</h2>
						<div class="card-body">
							<h5 class="card-title">Followers: {addCommas(artist2Data.followers)}</h5>
						</div>
						<img class="artist-image" src={artist2Data.strArtistThumb} alt="" />
						<div class="card-body">
							<div className="container">
								<div className="row justify-content-around">
									<div className="col-md-6">
										<p class="card-text">
											Active Years: {artist2Data.intFormedYear} - {artist2Data.strDisbanded}
										</p>
									</div>
									<div className="col-md-6">
										<p class="card-text">Genre: {artist2Data.strGenre}</p>
									</div>
								</div>
								<br />
								<div className="row justify-content-around">
									<div className="col-md-6">
										<p class="card-text">Members: {artist2Data.intMembers}</p>
									</div>
									<div className="col-md-6">{artist2Data.strCountry !== "" && <p class="card-text">From: {artist2Data.strCountry}</p>}</div>
								</div>
								<br />
								<div className="row justify-content-around">
									<div className="col-md-6">
										<p class="card-text">
											Top Track:{" "}
											<a href={songLink2} id="top-track" class="card-link">
												{artist2Data.topTrack}
											</a>
										</p>
									</div>
									<div className="col-md-6">
										<p class="card-text">
											Top Album:{" "}
											<a href={albumLink2} id="top-album" class="card-link">
												{artist2Data.topAlbum}
											</a>
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}

export default ArtistComparePage
