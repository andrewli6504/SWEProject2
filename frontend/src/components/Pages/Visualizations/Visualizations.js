import React, { useState, useEffect, Fragment, useRef } from "react";
import 'antd/dist/antd.css';
import NavBar from '../../NavBar';
import V1 from './V1';
import V2 from './V2';
import V3 from './V3';
import V4 from './V4';
import V5 from './V5';
import V6 from './V6';


function Visualizations() {

    return (
        <Fragment>
            <NavBar/>
            <h1 className="text-center" style={{ marginTop: '30px'}}> Our Visualizations</h1>
            <V1/>
            <V2/>
            <V3/>
            <h1 className="text-center" style={{ marginTop: '30px'}}> Provider Visualizations</h1>
            <V4/>
            <V5/>
            <V6/>
        </Fragment>
    )
}

export default Visualizations;
