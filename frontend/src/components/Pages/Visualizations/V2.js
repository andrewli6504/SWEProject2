import React, { Component } from "react";
import { Pie, PieChart, Tooltip } from "recharts";

class V2 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: null
        }
    }

    async componentDidMount() {
        const requestOptions = {
            method: "GET",
            headers: {},
        }

        var link = `https://spotifynder.com/api/songs`
        const response = await fetch(link, requestOptions)
        const data = await response.json()
        this.setState({
            data: data.message
        })
    }

    getSongsData() {
        let songData = []
        for (let song of this.state.data) {
            let dict = {
                name: song.name,
                genre: song.genres
            }
            songData.push(dict)
        }
        
        let data = []
        for(let song of songData) {
            let g = data.find(e => e.genre === song.genre)
            if(g === undefined) {
                var randomColor = "#" + Math.floor(Math.random() * 16777215).toString(16);
                let dict = {
                    genre: song.genre,
                    value: 1,
                    fill: randomColor
                }
                data.push(dict)
            }
            else {
                g.value++
            }
        }
        data.sort(function(a, b) {
            return b.value - a.value
        })
        return data
    }

    render() {
        let data = this.state.data;

        return (data != null) ? (
            <div>
                <h3 className="text-center" style={{ marginTop: '30px'}}> Frequency of Each Song Genre </h3>
                <div style={{ display: "flex", alignItems: "center", justifyContent: "center"}} >
                    <div>
                        <PieChart
                            width={1000}
                            height={650}
                            margin={"auto"}
                            >
                            <Pie 
                                dataKey="value"
                                nameKey="genre"
                                isAnimationActive={true}
                                data={this.getSongsData()}
                                outerRadius={300}
                                fill="#1DB954"
                                label
                            />
                            <Tooltip />
                        </PieChart>
                    </div>
                </div>
            </div>
        ) : (
            <div>
                <h3 className="text-center"> Frequency of Each Song Genre </h3>
                <p className="text-center"> Loading Data...</p>
            </div>
        );
    }
}

export default V2;
