import React, { useState, useEffect, Fragment, useRef, Component, PureComponent} from "react";
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

class V1 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: null
        }
    }

    async componentDidMount() {
        const requestOptions = {
            method: "GET",
            headers: {},
        }
        const response = await fetch(`https://readyrecipes.me/api/recipes`, requestOptions)
        const data = await response.json()
        console.log(data)
        console.log("yes")
        this.setState({
            data: data.data
        })
    }

    getValues() {
        var values = []
        
        for(var score = 70; score <= 100; score += 1) {
            let dict = {
                score: score,
                price: 0,
                time: 0
            }
            let count = 0
            for(let recipe of this.state.data){
                if(recipe.healthScore == score)
                {
                    dict.price += recipe.pricePerServing
                    dict.time += recipe.cookingMinutes + recipe.preparationMinutes
                    count++
                }
            }
            dict.price = dict.price / count
            dict.time = dict.time / count
            if(count != 0)
                values.push(dict)
        }

        return values;
    }

    render() {
        let data = this.state.data;

        return (data != null) ? (
            <div>
                <h3 className="text-center" style={{ marginTop: '30px'}}>Price per Serving and Cook time vs Health Score</h3>
                <div style={{ display: "flex", alignItems: "center", justifyContent: "center"}} >
                <LineChart
                    width={750}
                    height={400}
                    data={this.getValues()}
                >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="score" />
                    <YAxis yAxisId="left" />
                    <YAxis yAxisId="right" orientation="right" />
                    <Tooltip />
                    <Legend />
                    <Line yAxisId="left" type="monotone" dataKey="price" stroke="#8884d8" activeDot={{ r: 8 }} />
                    <Line yAxisId="right" type="monotone" dataKey="time" stroke="#82ca9d" />
                </LineChart>
                </div>
            </div>
        ) : (
            <div>
                <h3 className="text-center"> Frequency of Artist Genre by Decade</h3>
                <p className="text-center"> Loading Data...</p>
            </div>
        );
    }
}

export default V1;