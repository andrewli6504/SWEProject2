"""
A simple selenium test example written by python
"""

import unittest
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException

class TestTemplate(unittest.TestCase):
    """Include test cases on a given url"""

# 
# Initialize Driver
# 

    def setUp(self):
        """Start web driver"""
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.driver = webdriver.Chrome(options=chrome_options)
        self.driver.implicitly_wait(10)

# 
# Stop Driver once Tests Completed
# 

    def tearDown(self):
        """Stop web driver"""
        self.driver.quit()

# 
# Splash Page Button Tests
# 

    # def test_case_1(self):
    #     """Find and click songs button on Splash Page"""
    #     try:
    #         self.driver.get("https://spotifynder.com")
    #         songs_button = self.driver.find_element_by_id("songs_nav")
    #         songs_button.click()
    #         self.assertIn("https://spotifynder.com/songs", self.driver.current_url)
    #         self.driver.close()
    #     except NoSuchElementException as ex:
    #         self.fail(ex.msg)

    def test_case_2(self):
        """Find and click artists button on Splash Page"""
        try:
            self.driver.get("https://spotifynder.com")
            songs_button = self.driver.find_element_by_id("artists_nav")
            songs_button.click()
            self.assertIn("https://spotifynder.com/artists", self.driver.current_url)
            self.driver.close()
        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_case_3(self):
        """Find and click albums button on Splash Page"""
        try:
            self.driver.get("https://spotifynder.com")
            songs_button = self.driver.find_element_by_id("albums_nav")
            songs_button.click()
            self.assertIn("https://spotifynder.com/albums", self.driver.current_url)
            self.driver.close()
        except NoSuchElementException as ex:
            self.fail(ex.msg)

# 
# About Page Tests
# 

    def test_case_4(self):
        """Find and click profile picture on About Page"""
        try:
            self.driver.get("https://spotifynder.com/about")
            songs_button = self.driver.find_element_by_id("nathanljackson112_img")
            songs_button.click()
            self.assertIn("https://www.linkedin.com", self.driver.current_url)
            self.driver.close()
        except NoSuchElementException as ex:
            self.fail(ex.msg)


    def test_case_5(self):
        """Find and click postman on about Page"""
        try:
            self.driver.get("https://spotifynder.com/about")
            songs_button = self.driver.find_element_by_id("postman_link")
            songs_button.click()
            self.assertIn("https://documenter.getpostman.com/", self.driver.current_url)
            self.driver.close()
        except NoSuchElementException as ex:
            self.fail(ex.msg)


    def test_case_6(self):
        """Find and click gitlab on about page"""
        try:
            self.driver.get("https://spotifynder.com/about")
            songs_button = self.driver.find_element_by_id("gitlab_link")
            songs_button.click()
            self.assertIn("https://gitlab.com/ckite2k/SWEProject2", self.driver.current_url)
            self.driver.close()
        except NoSuchElementException as ex:
            self.fail(ex.msg)

# 
# NavBar Link Tests
# 

    def test_case_8(self):
        """Find and click home link in NavBar"""
        try:
            self.driver.get("https://spotifynder.com")
            songs_button = self.driver.find_element_by_id("home_link")
            songs_button.click()
            self.assertIn("https://spotifynder.com", self.driver.current_url)
            self.driver.close()
        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_case_9(self):
        """Find and click songs link in NavBar"""
        try:
            self.driver.get("https://spotifynder.com/")
            songs_button = self.driver.find_element_by_id("songs_link")
            songs_button.click()
            self.assertIn("https://spotifynder.com/songs", self.driver.current_url)
            self.driver.close()
        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_case_10(self):
        """Find and click artists link in NavBar"""
        try:
            self.driver.get("https://spotifynder.com")
            songs_button = self.driver.find_element_by_id("artists_link")
            songs_button.click()
            self.assertIn("https://spotifynder.com/artists", self.driver.current_url)
            self.driver.close()
        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_case_11(self):
        """Find and click album link in NavBar"""
        try:
            self.driver.get("https://spotifynder.com")
            songs_button = self.driver.find_element_by_id("albums_link")
            songs_button.click()
            self.assertIn("https://spotifynder.com/albums", self.driver.current_url)
            self.driver.close()
        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_case_12(self):
        """Find and click about link in NavBar"""
        try:
            self.driver.get("https://spotifynder.com")
            songs_button = self.driver.find_element_by_id("about_link")
            songs_button.click()
            self.assertIn("https://spotifynder.com/about", self.driver.current_url)
            self.driver.close()
        except NoSuchElementException as ex:
            self.fail(ex.msg)

# 
# Model Page Tests
# 

    def test_case_13(self):
        """Find and click on row in songs page"""
        try:
            self.driver.get("https://spotifynder.com/songs")
            rows = self.driver.find_elements_by_tag_name("tr")
            if(len(rows) > 0): 
                rows[0].click()
                self.assertIn("https://spotifynder.com/songs", self.driver.current_url)
            else:
                self.assertTrue(False)
            self.driver.close()
        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_case_14(self):
        """Find and click on row in artist page"""
        try:
            self.driver.get("https://spotifynder.com/artists")
            rows = self.driver.find_elements_by_tag_name("tr")
            if(len(rows) > 0): 
                rows[0].click()
                self.assertIn("https://spotifynder.com/artists", self.driver.current_url)
            else:
                self.assertTrue(False)
            self.driver.close()
        except NoSuchElementException as ex:
            self.fail(ex.msg)

# 
# Artist Instance Page Tests
# 

    def test_case_15(self):
        """Find and click top track on Artist Instance Page"""
        try:
            self.driver.get("https://spotifynder.com/artists")
            rows = self.driver.find_elements_by_tag_name("tr")
            if(len(rows) > 0): 
                rows[0].click()
                self.assertIn("https://spotifynder.com/artist", self.driver.current_url)
                # track_link = self.driver.find_element_by_css_selector('#top-track.card-link')
                # track_link.click()
                # self.assertIn("https://spotifynder.com/song", self.driver.current_url)
            self.driver.close()
        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_case_16(self):
        """Find and click top album on Artist Instance Page"""
        try:
            self.driver.get("https://spotifynder.com/artists")
            rows = self.driver.find_elements_by_tag_name("tr")
            if(len(rows) > 0): 
                rows[0].click()
                self.assertIn("https://spotifynder.com/artist", self.driver.current_url)
                # track_link = self.driver.find_elements_by_tag_name("a")
                # track_link[1].click()
                # self.assertIn("https://spotifynder.com/album", self.driver.current_url)
            self.driver.close()
        except NoSuchElementException as ex:
            self.fail(ex.msg)

# 
# Song Instance Page Tests
# 

    def test_case_17(self):
        """Find and click song's artist on Song Instance Page"""
        try:
            self.driver.get("https://spotifynder.com/songs")
            rows = self.driver.find_elements_by_tag_name("tr")
            if(len(rows) > 0): 
                rows[0].click()
                self.assertIn("https://spotifynder.com/song", self.driver.current_url)
                # track_link = self.driver.find_element_by_id("track-artist")
                # track_link.click()
                # self.assertIn("https://spotifynder.com/artist", self.driver.current_url)
            self.driver.close()
        except NoSuchElementException as ex:
            self.fail(ex.msg)

    def test_case_18(self):
        """Find and click song album on Song Instance Page"""
        try:
            self.driver.get("https://spotifynder.com/songs")
            rows = self.driver.find_elements_by_tag_name("tr")
            if(len(rows) > 0): 
                rows[0].click()
                self.assertIn("https://spotifynder.com/song", self.driver.current_url)
                # track_link = self.driver.find_element_by_id("track-album")
                # track_link.click()
                # self.assertIn("https://spotifynder.com/album", self.driver.current_url)
            self.driver.close()
        except NoSuchElementException as ex:
            self.fail(ex.msg)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestTemplate)
    result = unittest.TextTestRunner(verbosity=2).run(suite)

    if result.wasSuccessful():
        exit(0)
    else:
        exit(1)
